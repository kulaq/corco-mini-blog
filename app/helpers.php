<?php

use Illuminate\Support\Facades\Route;

if (! function_exists('active_link')) {
    function activeLink(string $name, string $active = 'active'): string
    {
        return Route::is($name) ? $active : '';
    }
}

if (! function_exists('alert')) {
    function alert(string $value)
    {
        session(['alert' => $value]);
    }
}
