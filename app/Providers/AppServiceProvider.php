<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        \Illuminate\Support\Facades\View::share('date', date('Y'));

        \Illuminate\Support\Facades\View::composer('name', function($view) {
            $view->with('balance', '4568');
        });

        Paginator::useBootstrapFive();
    }
}
