<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;


class PostController extends Controller
{
        public function index()
        {
            return "Hello world";
        }

        public function create()
        {
            return "create post";
        }

        public function store(Request $request)
        {

        }

        public function show($post)
        {
            return "show/{$post}";
        }

        public function edit()
        {

        }

        public function update()
        {

        }

        public function delete()
        {

        }
}
