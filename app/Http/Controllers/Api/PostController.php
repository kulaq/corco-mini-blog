<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function index()
    {
        return "Hello index";
    }

    public function show($post)
    {
        return "show {$post}";
    }


    public function like($post)
    {
        return "like {$post} +1";
    }

}
