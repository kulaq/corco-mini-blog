<?php

namespace App\Http\Controllers\Posts;


class Builder
{
    public function create($attributes)
    {
        dd($attributes);
    }
}


class Mamph
{
    protected array $attributes = [];

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    public static function __callStatic($method, $params)
    {
        return (new Builder)->{$method}(...$params);
    }

    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

}

class Order extends Mamph
{

}


//$order = new Order();
//$order->setAttribute('foo', 'bar');
//
//$order->name = 'chuvak';
//$order->password = 'pa$$word';

$order = Order::create([
   'foo' => 'bar',
]);


print_r($order);
