<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
//use App\Http\Requests\Post\StorePostRequest;
use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function alert;


class PostController extends Controller
{
    public function index()
    {
//        $post = (object) [
//            'id' => 1,
//            'title' => 'Lorem ipsum dolor sit amet.',
//            'content' => 'Lorem ipsum <strong>dolor</strong> sit amet, consectetur adipisicing elit. Ratione, voluptates.',
//        ];
//
//        $posts = array_fill(0, 10, $post);

        $posts = Post::query()->paginate(12);

        return view('user.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('user.posts.create');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => ['required', 'string', 'max:100'],
            'content' => ['required', 'string', 'max:10000'],
            'published_at' => ['nullable', 'string', 'date'],
            'published' => ['nullable', 'boolean'],
        ]);

        $post = Post::query()->firstOrCreate([
            'user_id' => User::query()->value('id'),
            'title' => $validated['title'],
            ], [
            'content' => $validated['content'],
            'published_at' => new Carbon ($validated['published_at'] ?? null),
            'published' => $validated['published'] ?? false,
        ]);


        dd($post->toArray());


        alert(__('Сохранено'));
//
//        return redirect()->route('user.posts.show', 1);
    }

    public function show($post)
    {
        $post = (object) [
            'id' => 1,
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum <strong>dolor</strong> sit amet, consectetur adipisicing elit. Ratione, voluptates.',
        ];

        $errors = [];

        return view('user.posts.show', compact('post', 'errors'));
    }

    public function edit($post)
    {
        $post = (object) [
            'id' => 1,
            'title' => 'Lorem ipsum dolor sit amet.',
            'content' => 'Lorem ipsum <strong>dolor</strong> sit amet, consectetur adipisicing elit. Ratione, voluptates.',
        ];

        return view('user.posts.edit', compact('post'));
    }

    public function update(StorePostRequest $request, $post)
    {
//        $title = $request->input('title');
//        $content = $request->input('content');
        $validated = $request->validated();
        dd($validated);
        alert(__('Сохранено'));


        return redirect()->back();

    }

    public function delete($post)
    {

        return redirect()->route('user.posts');

    }

    public function fakeValue() // генерирую фейковые данные
    {
        for ($i = 0; $i < 99; $i++) {
            $post = Post::query()->create([
                'user_id' => User::query()->value('id'),
                'title' => fake()->sentence(),
                'content' => fake()->paragraph(),
                'published_at' => fake()->dateTimeBetween(now()->subYear(), now()),
                'published' => true,
            ]);
        }

        print_r('oK');
    }

}
