<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.index');
    }

    public function store(Request $request)
    {
//        $data = $request->all();
//        $name = $request->only('name');
//        $except = $request->except('name', 'email');

//        $input = $request->input('name');
//        $agreement = $request->boolean('agreement');
//        $has = $request->has('name'); // проверяет есть ли этот параметр
//        $filled = $request->filled('name'); //проверяет есть ли значение у этого параметра

        $validated = $request->validate([
           'name' => ['required', 'string', 'max:50'],
           'email' => ['required', 'string', 'max:50', 'unique:users', 'email'],
           'password' => ['required', 'string', 'min:7','max:50', 'confirmed'],
           'agreement' => ['accepted'],
        ]);

        $user = User::query()->create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => bcrypt($validated['email']),
        ]);

//        dd($user->toArray());

        return redirect()->route('user');
    }
}
