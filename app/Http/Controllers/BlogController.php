<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $categories = [
          null => __('Все категории'),
          1 => __('Первая категория'),
          2 => __('Вторая категория'),
        ];
//        $posts = Post::all('id', 'title', 'published_at');
//        dd($posts->toArray());

//        $posts = Post::query()->get(['id', 'title', 'published_at']);
//        $posts = Post::query()->limit(12)->get();

//        $validated = $request->validate([
//           'limit' => ['nullable', 'integer', 'min:1', 'max:100'],
//           'page' => ['nullable', 'integer', 'min:1', 'max:100'],
//        ]);
//
//        $page = $validated['page'] ?? 1;
//        $limit = $validated['limit'] ?? 9;
//        $offset = $limit * ($page - 1);

//        $posts = Post::query()->limit($limit)->offset($offset)->get(); //кастом
//        $posts = Post::query()->orderBy('published_at', 'desc')->paginate(12); // можно проще

//        $posts = Post::query()->paginate($limit);
        $posts = Post::query()->latest('published_at')->paginate(12);
        return view('blog.index', compact('posts', 'categories'));
    }

    public function show(Request $request, Post $post)
    {
          // select * from posts order by published_at ask limit 1
//        $post = Post::query()->oldest('published_at')->first(['id', 'title']);
//        $post = Post::query()->oldest('published_at')->firstOrFail(['id', 'title']);

        // select id, title, content from posts where id = 123, limit 1
//        $post = Post::query()->oldest('published_at')->find($post, ['id', 'title']);
//        $post = Post::query()->oldest('published_at')->findOrFail($post, ['id', 'title']);

        //select id, title, published_at from posts where id in (1, 2, 3, 4)
//        $post = Post::query()->find([1, 2, 3, 4], ['id', 'title', 'published_at']);

//        $post = Post::query()->findOrFail($post);
        return view('blog.show', compact('post'));
    }
}
