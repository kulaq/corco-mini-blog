<textarea {{ $attributes->class([
    'form-control',
])->merge([
    'type' => 'text'
]) }}></textarea>
