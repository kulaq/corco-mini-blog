@props(['post' => null])

<x-errors />

<x-form {{ $attributes }} >
    <x-form-item>
        <x-label required>
            {{ __('Название поста') }}
        </x-label>

        <x-input name="title"  autofocus/>
        <x-error name="title"/>
    </x-form-item>

    <x-form-item>
        <x-label required>
            {{ __('Содержание поста') }}
        </x-label>

        <input id="content" type="hidden" value="{{ $post->content ?? '' }}" name="content">
        <trix-editor input="content" name="content" ></trix-editor>
        <x-error name="content"/>
    </x-form-item>


    <x-form-item>
        <x-label required>
            {{ __('Дата публикации') }}
        </x-label>

        <x-input name="published_at" placeholder="dd.mm.yyyy"/>
        <x-error name="published_at"/>
    </x-form-item>

    <x-form-item>
        <x-checkbox name="published">
            Опубликовано
        </x-checkbox>
    </x-form-item>


    {{ $slot }}

</x-form>

